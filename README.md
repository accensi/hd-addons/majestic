### Important
---
- This mod requires [BulletLib](https://gitlab.com/accensi/hd-addons/hdbulletlib) and [AceCoreLib](https://gitlab.com/accensi/hd-addons/acecorelib).
- Supports ACL's spawn scaling.

### Notes
---
- The loadout codes are `maj` and `mjm` for the weapon and mag respectively.
- Configuration codes are:
	- `accel`: Accelerator. Makes the gun charge a little faster.
### Mechanics
---
- The weapon requires both a battery and a drum to fire a charged shot. Otherwise only a drum is required.
- Holding primary fire charges a shot. The higher the charge, the higher the projectile's velocity and explosion damage. Charging is not necessary and the gun can be quickfired in a pinch.